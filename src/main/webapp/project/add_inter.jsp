<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<%@ include file="../common/comm_css.jsp"%>
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>添加接口</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<style type="text/css">
			#cover{ 
				display:none; 
				position:fixed; 
				z-index:1; 
				top:0; 
				left:0; 
				width:100%; 
				height:100%; 
				background:rgba(0, 0, 0, 0.44); 
			} 
			#coverShow{ 
				display:none; 
				position:fixed; 
				z-index:2; 
				top:50%; 
				left:50%; 
				border:0px solid #127386; 
			} 
		</style>
	</head>

	<body class="gray-bg">
		<div id="cover"></div> 
		<div id="coverShow"> 
			<img src="${pageContext.request.contextPath}/img/loaders/4.gif"/>
			<p>请稍后......</p>
		</div>
		<div class="wrapper wrapper-content animated">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>添加接口<small></small></h5>
						</div>
						<div class="ibox-content">
							<div class="form-horizontal">
								<div class="row">
									<div class="col-lg-8 col-xs-9 col-md-9">
										<table class="table table-bordered dataTables-example" style="margin-bottom: 0px;">
											<tbody>
												<tr>
													<td class="col-lg-1 col-xs-1 col-md-2 text-right"><b>编号</b></td>
													<td class="col-lg-3 col-xs-4 col-md-3"><input type="text" class="form-control" id="interface_num"></td>
													<td class="col-lg-1 col-xs-1 col-md-2 text-right">接口名称</td>
													<td colspan="3"><input type="text" class="form-control" id="interface_name"></td>
												</tr>
												<tr>
													<td colspan="6"><textarea id="interface_remark" class="form-control" rows="3" style="resize: none;" placeholder="接口描述内容"></textarea></td>
												</tr>
												<tr>
													<td class="col-lg-1 col-xs-1 col-md-2 text-right">请求方式</td>
													<td class="col-lg-3 col-xs-4 col-md-3"> 
														<select class="form-control" id="interface_type">
															<option value="1">Get</option>
															<option value="2">Post</option>
															<option value="3">Put</option>
															<option value="4">Delete</option>
														</select>
														<!-- 
														<div class="radio">
                                					    	<label><input style="top: 5px;" type="radio" name="interface_type" checked="checked" value="1"> <i></i> GET</label>
                            					    	 	<label style="margin-left: 20px;"><input style="top: 5px;" type="radio" name="interface_type" value="2"> <i></i> Ajax Post</label>
														</div>
														 -->
													</td>
													<td class="col-lg-1 col-xs-1 col-md-1 text-right">URL</td>
													<td colspan="2"><input type="text" class="form-control" id="interface_url"></td>
												</tr>
												<tr>
													<td class="text-right">请求参数</td>
													<td colspan="5" style="padding: 2px 2px;">
														<table class="table table-bordered dataTables-example" style="margin-bottom: 0px;">
															<thead>
																<tr>
																	<th class="col-lg-2 col-xs-2 col-md-2">参数名称</th>
																	<th class="col-lg-2 col-xs-2 col-md-2">参数类型</th>
																	<th class="text-center col-lg-1 col-xs-1 col-md-1">必填</th>
																	<th>参数说明<a id="p_add" class="btn btn-primary" style="padding: 0 10px; font-size: 10px;margin-bottom: 0px;height: 18px;float: right;" data-toggle="modal" data-target="#myModal4">批量添加</a></th>
																	<th style="width: 10%; text-align: center;"><a id="add" class="btn btn-primary" style="padding: 0 10px; font-size: 10px;margin-top:1px;margin-bottom: 0px;height: 18px;">添加</a></th>
																</tr>
															</thead>
															<tbody id="c_listgroup">
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td class="text-right">返回对象</td>
													<td colspan="4">
														<textarea class="form-control" rows="10" style="resize: none;" id="return_text"></textarea>
													</td>
												</tr>
												<tr>
													<td class="text-right">响应码</td>
													<td colspan="4">
														<textarea class="form-control" rows="3" style="resize: none;" id="return_code"></textarea>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-lg-4 col-xs-3 col-md-3">
										<a id="img_url" class="fancybox" href="${pageContext.request.contextPath}/img/wu.png">
											<img id="img" alt="image" src="${pageContext.request.contextPath}/img/wu.png" style="width: 100%;" />
										</a>
										<div style="background:#F00; height: 0px;width: 0;">
											<form name="upload1" action="/uploadFile.xco" method="post" style="margin:0px 0px;width: 100%;height: 30px;" enctype="multipart/form-data">
												<input type="file" name="file" id="fileField1" style="width: 100%;height: 30px;" onchange="select1(this);" value="选择图片" alt="点击选择图片" />
												<input type="hidden" id="file_url">
											</form>
											<br class="clear" />
										</div>
										<a id="add_img" class="btn btn-primary" style="width: 100%;">上传图片</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mar-t-20" style="text-align: center;">
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="submit">提交</button>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="exit">返回</button>
			</div>
		</div>
		<div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">批量生产参数</h4>
                        <small>在文本域复制xml字符串。
                    </div>
                    <div class="modal-body" style="padding:10px;">
							<textarea id="txt" rows="5" class="form-control" style="width:100%;resize: none;"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary" id="sub" data-dismiss="modal">保存</button>
                    </div>
                </div>
            </div>
        </div>
	</body>

</html>
<script src="${pageContext.request.contextPath}/js/content.js?v=1.0.0"></script>
<script type="text/javascript">
	var pro_id = getURLparam("pro_id");
	var mod_id = getURLparam("module_id");
	
	var i = 0;
	var array = new Array();
	$(document).ready(function() {
		//图片
		$('.fancybox').fancybox({
			openEffect: 'none',
			closeEffect: 'none'
		});
		$('#add').click(function(){
			var html = '';
			html += '<tr id="tr_'+i+'">';
			html += '	<td class="col-lg-2"><input type="text" class="form-control" id="c_name'+i+'"></td>';
			html += '	<td class="col-lg-2"><input type="text" class="form-control" id="c_type'+i+'"></td>';
			html += '	<td class="col-lg-1 text-center" style="max-width: 5%;">';
			html += '		<label><input id="c_check'+i+'" type="checkbox" value="" style="margin-top: 7px;"> <i></i> </label>';
			html += '	</td>';
			html += '	<td><input type="text" class="form-control" id="c_remark'+i+'"></td>';
			html += '	<td class="text-center"><a class="btn btn-primary" style="margin-bottom: 0px;" onclick="del('+i+')">删除</a></td>';
			html += '</tr>';
			array.push(i);
			//alert(array);
			i++;
			$('#c_listgroup').append(html);
		});
		
		$("#p_add").click(function(){
			$("#txt").val("");
		})
		
		$("#sub").click(function(){
			var html = '';
			var txt = $("#txt").val();
			var xco = new XCO();
			txt = txt.replace(/>\s*?</g, "><"); 
			xco.fromXML(txt);
			var keys = getXCOKeyValue(xco);
			var html = '';
			for(var x = 0; x<keys.length;x++){
				html += '<tr id="tr_'+i+'">';
				html += '	<td class="col-lg-2"><input type="text" class="form-control" id="c_name'+i+'" value="'+keys[x].name+'"></td>';
				html += '	<td class="col-lg-2"><input type="text" class="form-control" id="c_type'+i+'" value="'+keys[x].type+'"></td>';
				html += '	<td class="col-lg-1 text-center" style="max-width: 5%;">';
				html += '		<label><input id="c_check'+i+'" type="checkbox" value="" style="margin-top: 7px;"> <i></i> </label>';
				html += '	</td>';
				html += '	<td><input type="text" class="form-control" id="c_remark'+i+'"></td>';
				html += '	<td class="text-center"><a class="btn btn-primary" style="margin-bottom: 0px;" onclick="del('+i+')">删除</a></td>';
				html += '</tr>';
				array.push(i);
				i++;
			}
			
			$('#c_listgroup').append(html);
		})
		
		$('#add_img').click(function(){
			$("#fileField1").trigger("click");
		})
	});
	
	function del(num){
		$('#tr_'+num).remove();
		array.remove(num);
		//alert(array);
	}
	
	//判断上传文件格式是否满足条件
	function isPicture(fileName){
	    if(fileName!=null && fileName !=""){
	  		//lastIndexOf如果没有搜索到则返回为-1
	  		if (fileName.lastIndexOf(".")!=-1) {
				var fileType = (fileName.substring(fileName.lastIndexOf(".")+1,fileName.length)).toLowerCase();
				var suppotFile = new Array();
				suppotFile[0] = "jpg";
	   			suppotFile[1] = "gif";
	   			suppotFile[2] = "bmp";
	   			suppotFile[3] = "png";
	   			suppotFile[4] = "jpeg";
				for (var i =0;i<suppotFile.length;i++) {
					if (suppotFile[i]==fileType) {
						return true;
					} else{
					 continue;
					}
				}
				layer.msg("文件类型不合法,只能是jpg、gif、bmp、png、jpeg类型！", {time: times, icon:no});
				return false;
			} else{
				layer.msg("文件类型不合法,只能是jpg、gif、bmp、png、jpeg类型！", {time: times, icon:no});
			  	return false;
			}
		}
	}
	
	function select1(e){
		if(e.value){
			var fileField1 = $("#fileField1").val();
			if(!isPicture(fileField1)){
				return;
			}
			var cover = document.getElementById("cover"); 
			var covershow = document.getElementById("coverShow"); 
			cover.style.display = 'block'; 
			covershow.style.display = 'block'; 
			$("form[name='upload1']").ajaxSubmit({
		    	dataType:'xml',
		      	success:function(data){
		        	var xco=new XCO();
		        	xco.fromXML0(data);
		      		if(xco.getCode() == 0){
		      			cover.style.display = 'none'; 
						covershow.style.display = 'none'; 
						var _dataList = xco.getXCOListValue("updateResult");
						if(_dataList.length > 0){
			      			$("#img").attr("src",_dataList[0].getStringValue("url"));
			      			$("#img_url").attr("href",_dataList[0].getStringValue("url"));
			      			$("#file_url").val(_dataList[0].getStringValue("url"));
						}
		      		}
		      	}
	        })
		}
	}
	var c_xco = new XCO();//组装参数xco
	function installXCO(){
		var xcoarray = new Array();
		for(var o = 0;o<array.length;o++){
			var xco1 = new XCO();
			var c_name = $("#c_name"+array[o]).val();
			if(c_name){
				xco1.setStringValue("c_name",c_name);
			}else{
				layer.msg("参数名称不能为空", {time: times, icon:no});
				return false;
			}
			
			var c_type = $("#c_type"+array[o]).val();
			if(c_type){
				xco1.setStringValue("c_type",c_type);
			}else{
				layer.msg("参数类型不能为空", {time: times, icon:no});
				return false;
			}
			
			var c_remark = $("#c_remark"+array[o]).val();
			if(c_remark){
				xco1.setStringValue("c_remark",c_remark);
			}else{
				layer.msg("参数说明不能为空", {time: times, icon:no});
				return false;
			}
			
			if($("#c_check"+array[o]).is(':checked')){
				xco1.setIntegerValue("c_check",1);//选中是1
			}else{
				xco1.setIntegerValue("c_check",0);//未选中是0
			}
			xcoarray.push(xco1);
		}
		if(xcoarray.length==0){
			layer.msg("请填写请求参数", {time: times, icon:no});
			return false;
		}
		c_xco.setXCOListValue("array", xcoarray);
		return true;
	}
	
	$("#submit").click(function(){
		var xco = new XCO();
		
		var interface_num = $("#interface_num").val();
		if(interface_num){
			xco.setStringValue("interface_num",interface_num);
		}else{
			layer.msg("请输入接口编号", {time: times, icon:no});
			return;
		}
		
		var interface_name = $("#interface_name").val();
		if(interface_name){
			xco.setStringValue("interface_name",interface_name);
		}else{
			layer.msg("请输入接口名称", {time: times, icon:no});
			return;
		}
		
		var interface_url = $("#interface_url").val();
		if(interface_url){
			xco.setStringValue("interface_url",interface_url);
		}else{
			layer.msg("请输入接口Url", {time: times, icon:no});
			return;
		}
		
		if(installXCO()){
			xco.setStringValue("interface_param",c_xco.toString());
		}else{
			return;
		}
		
	 	//var interface_type=$('input:radio[name="interface_type"]:checked').val();
	 	xco.setIntegerValue("interface_type",$("#interface_type").val());
	 	
	 	var interface_remark = $("#interface_remark").val();
	 	if(interface_remark){
			xco.setStringValue("interface_remark",interface_remark);
		}else{
			layer.msg("请输入接口描述", {time: times, icon:no});
			return;
		}
	 	
	 	var img_url = $("#file_url").val();
	 	if(img_url){
	 		xco.setStringValue("img_url",img_url);
	 	}
	 	
	 	xco.setLongValue("pro_id",pro_id);
	 	xco.setLongValue("mod_id",mod_id);
	 	
		var return_code = $("#return_code").val();
	 	if(return_code){
			xco.setStringValue("return_code",return_code);
		}else{
			layer.msg("请输入接口响应吗", {time: times, icon:no});
			return;
		}
	 	
		var return_text = $("#return_text").val();
	 	if(return_text){
			xco.setStringValue("return_text",return_text);
		}else{
			layer.msg("请输入接口返回对象", {time: times, icon:no});
			return;
		}
	 	
	 	var options = {
			url : "/manager/insertInterface.xco",
			data : xco,
			success : insertInterfaceCallBack
		};
		layer.confirm('是否添加该接口？', {
			title:'提示',
		  	btn: ['是的','不要'] //按钮
		}, function(){
			$.doXcoRequest(options);
		})
	});
	
	function insertInterfaceCallBack(data){
		if(data.getCode()!=0){
			layer.msg(data.getMessage(), {time: times, icon:no});
		}else{
			layer.msg("添加成功", {time: times, icon:ok});
			location.href="../project/interface.jsp?pro_id="+pro_id+"&&module_id="+mod_id;
		}
	}
	
	$("#exit").click(function(){
		location.href="../project/interface.jsp?pro_id="+pro_id+"&&module_id="+mod_id;
	});
</script>